from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    user_id = serializers.UUIDField(format='hex_verbose')
    login = serializers.CharField(max_length=100)
    name = serializers.CharField(max_length=255)
    user_type = serializers.CharField(max_length=4)