from django.shortcuts import render

from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User
from .serializers import UserSerializer

import base64
import json

class UserView(APIView):
    
    def get(self, request):
        user_id = self.request.query_params.get('id')
        
        user_obj = get_object_or_404(User.objects.all(), user_id=user_id)
        user_serialized = UserSerializer(user_obj, many=False)
        return Response({"user_id": user_serialized.data.get('user_id'), "user_type": user_serialized.data.get('user_type'), "name": user_serialized.data.get('name')})

    
    def post(self, request):
        login = request.data.get('login')

        user_obj = get_object_or_404(User.objects.all(), login=login)
        user_serialized = UserSerializer(user_obj, many=False)

        JWT_Header_Encoded = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
        JWT_Payload_Obj = {
            "user_id": user_serialized.data.get('user_id'),
            "user_type": user_serialized.data.get('user_type')
        }
        JWT_Payload_String = json.dumps(JWT_Payload_Obj)
        JWT_Payload_Encoded = base64.b64encode(str.encode(JWT_Payload_String)).decode()

        JWT_Verify = 'ey' # Здесь я пока умышленно не делаю верифицирующую часть как положено,
        #                   а оставляю заглушку. В настоящем JWT она служет подтверждением того,
        #                   что токен не поддельный, а выдан по секрету нашим сервером.
        #                   Но в рамках данного примера это не так важно

        return Response({"token": JWT_Header_Encoded+'.'+JWT_Payload_Encoded+'.'+JWT_Verify})