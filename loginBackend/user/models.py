import uuid
from django.db import models

# Create your models here.

class User(models.Model):
    # primary_key=True   - обозначаю что uuid является первичным ключом для записей пользователей,
    #                      т.е. по ним я буду искать и узнавать.
    # default=uuid.uuid4 - при создании записи, по умолчанию (default), задается некоторое значение.
    #                      В данном случае генерируется новый uuid-идентификатор (https://habr.com/ru/company/vk/blog/522094/)
    # editable=False     - и этот идентификатор нельзя менять, ведь мы по ним нашего пользователя определяем
    user_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    login = models.CharField(max_length=100)
    name = models.CharField(max_length=255)
    user_type = models.CharField(max_length=4)