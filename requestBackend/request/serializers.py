from rest_framework import serializers
from .models import Request

class RequestSerializer(serializers.Serializer):
    request_id = serializers.UUIDField(format='hex_verbose', required=False)
    student_id = serializers.UUIDField(format='hex_verbose')
    executor_id = serializers.UUIDField(format='hex_verbose', required=False, allow_null=True)
    title = serializers.CharField(max_length=300)
    
    def create(self, validated_data):
        return Request.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.student_id = validated_data.get('student_id', instance.student_id)
        instance.executor_id = validated_data.get('executor_id', instance.executor_id)
        instance.title = validated_data.get('title', instance.title)
        instance.save()
        return instance