from django.shortcuts import render

from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Request
from .serializers import RequestSerializer

import base64
import json
import requests

    # и опять же, в данном примере я опускаю момент с проверкой jwt-токена на честность
    # за вас скорее всего это может сделать какая-нибудь библиотека по работе c jwt
def parseJWT(token):
    jwt_token = token.split(' ', 2)[1] # отрезаем ключевое слово 'bearer' из заголовка (он имеет вид 'bearer ey...', то есть разделяем по пробелу)
    jwt_payload_b64 = jwt_token.split('.', 3)[1]
    jwt_payload_str = base64.b64decode(str.encode(jwt_payload_b64)).decode()
    jwt_payload = json.loads(jwt_payload_str)

    return jwt_payload
    
class RequestView(APIView):
    def get(self, request):
        # Достаю из заголовка Authorization jwt-токен, чтобы из него узнать id пользоватея
        auth_header = request.headers['Authorization']
        jwt_payload = parseJWT(auth_header)

        user_id = jwt_payload['user_id']
        user_type = jwt_payload['user_type']

        if (user_type == 'STUD'):
            requests_objs = Request.objects.filter(student_id=user_id)     # для студента выводим все те записи, которые создал он
        elif (user_type == 'EXEC'):
            free = self.request.query_params.get('free') # Смотрим, есть ли в запросе GET-параметр "free"
            if (free): requests_objs = Request.objects.filter(executor_id=None) # если есть пометка "free", то выдаем исполнителю записи, которые ещё на приняты
            else: requests_objs = Request.objects.filter(executor_id=user_id)    # иначе выдаваем те записи, на которые пользователь назначен исполнителем

        requests_serialized = RequestSerializer(requests_objs, many=True)
        for req in requests_serialized.data:
            # Делаем запрос к другому сервису за информацией по студенту из заявки
            r = requests.get('http://127.0.0.1/api/users/?id='+req['student_id'])
            student_info = r.json()
            # Добавляем к записи дополниетльное поле - полученное имя
            req['student_name'] = student_info['name']

            # Если у записи есть назначенный исполнитель, то уточняем и его имя
            if (req['executor_id']):
                r = requests.get('http://127.0.0.1/api/users/?id='+req['executor_id'])
                executor_info = r.json()
                req['executor_name'] = executor_info['name']


        return Response({"requests": requests_serialized.data})


    def post(self, request):
        title = request.data.get('title')

        # Достаю из заголовка Authorization jwt-токен, чтобы из него узнать id пользоватея
        auth_header = request.headers['Authorization']
        jwt_payload = parseJWT(auth_header)

        user_id = jwt_payload['user_id']
        user_type = jwt_payload['user_type']

        new_request = {
            'student_id': user_id,
            'executor_id': None,
            'title': title
        }
        serializer = RequestSerializer(data=new_request)
        if serializer.is_valid(raise_exception=True):
            request_saved = serializer.save()

        return Response({"added": request_saved.pk})

    def patch(self, request):
        # Достаю из заголовка Authorization jwt-токен, чтобы из него узнать id пользоватея
        auth_header = request.headers['Authorization']
        jwt_payload = parseJWT(auth_header)

        user_id = jwt_payload['user_id']
        user_type = jwt_payload['user_type']

    
        if (user_type == 'EXEC'):   # Этот запрос доступен только для исполнителей
            request_id = self.request.query_params.get('id')
            request_obj = get_object_or_404(Request.objects.all(), request_id=request_id)

            data_to_update = {
                'executor_id': user_id # Устанавливаем в прототипе наших данных в поле исполнить идентификатор нашего пользователя 
            }

            # Отправляем data_to_update на добавление к объекту в базе данных request_obj
            serializer = RequestSerializer(instance=request_obj, data=data_to_update, partial=True)
            if serializer.is_valid(raise_exception=True):
                article_saved = serializer.save()

            return Response({
                "success": "request updated successfully"
            })

        else: # Если наш пользователь не того типа(роли)
            return Response({ "forbidden": "not executor" })