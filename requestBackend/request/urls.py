from django.urls import path
from .views import RequestView
app_name = "request"
# app_name will help us do a reverse look-up latter.

urlpatterns = [
    path('requests/', RequestView.as_view()),
]